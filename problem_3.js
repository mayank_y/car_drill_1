function problem3(inv) {
  if (
    typeof inv == "string" ||
    typeof inv == "boolean" ||
    typeof inv == "number" ||
    typeof inv == "symbol" ||
    inv == null
  ) {
    inv == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof inv}, send a valid input`);
    return;
  } else if (inv.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  let arr = [];
  for (let ele in inv) {
    let obe = inv[ele];
    arr.push(obe.car_model);
  }

  arr = arr.sort();
  console.log("all the cars in ascending order : ");
  for (let i = 0; i < arr.length; i++) {
    console.log(arr[i]);
  }
}

module.exports = problem3;
