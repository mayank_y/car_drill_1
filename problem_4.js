let carSortedByYear = [];

function problem4(inv) {
  if (
    typeof inv == "string" ||
    typeof inv == "boolean" ||
    typeof inv == "number" ||
    typeof inv == "symbol" ||
    inv == null
  ) {
    inv == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof inv}, send a valid input`);
    return;
  } else if (inv.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let ele in inv) {
    let obe = inv[ele];
    carSortedByYear.push(obe.car_year);
  }

  carSortedByYear = carSortedByYear.sort();
  console.log(`years of all the cars:`);
  for (let i = 0; i < carSortedByYear.length; i++) {
    console.log(carSortedByYear[i]);
  }
  return carSortedByYear;
}

module.exports = { problem4, carSortedByYear };
