function problem6(inv) {
  if (
    typeof inv == "string" ||
    typeof inv == "boolean" ||
    typeof inv == "number" ||
    typeof inv == "symbol" ||
    inv == null
  ) {
    inv == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof inv}, send a valid input`);
    return;
  } else if (inv.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  // A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

  let bmwAudiCars = [];
  for (let index in inv) {
    let object = inv[index];
    if (object.car_make == "BMW" || object.car_make == "Audi") {
      bmwAudiCars.push(object.car_model);
    }
  }
  console.log(
    `${bmwAudiCars.length} cars are there in the lot. These are the models (in JSON string format)`
  );
  console.log(JSON.stringify(bmwAudiCars));
}

module.exports = problem6;
