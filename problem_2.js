function problem2(inv) {
  if (
    typeof inv == "string" ||
    typeof inv == "boolean" ||
    typeof inv == "number" ||
    typeof inv == "symbol" ||
    inv == null
  ) {
    inv == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof inv}, send a valid input`);
    return;
  } else if (inv.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  let obe = inv[inv.length - 1];
  console.log(`last car is ${obe.car_make} ${obe.car_model}`);
}

module.exports = problem2;
