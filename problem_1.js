let problem1 = (inv) => {
  if (
    typeof inv == "string" ||
    typeof inv == "boolean" ||
    typeof inv == "number" ||
    typeof inv == "symbol" ||
    inv == null
  ) {
    inv == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof inv}, send a valid input`);
    return;
  } else if (inv.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let ele in inv) {
    let obj = inv[ele];
    if (obj.id == 33) {
      console.log(
        `Car ${obj.id} is a car ${obj.car_year} ${obj.car_make} ${obj.car_model}`
      );
      return;
    }
  }
};
//problem1(inv);

module.exports = problem1;
