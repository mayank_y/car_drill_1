let carYearData = [];
function problem5(inv) {
  if (
    typeof inv == "string" ||
    typeof inv == "boolean" ||
    typeof inv == "number" ||
    typeof inv == "symbol" ||
    inv == null
  ) {
    inv == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof inv}, send a valid input`);
    return;
  } else if (inv.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let i = 0; i < inv.length; i++) {
    let obe = inv[i];
    console.log(obe.car_year);
    obe.car_year < 2000
      ? carYearData.push([obe.car_make, obe.car_model])
      : true;
  }
  console.log(
    "Number of cars with year less than 2000 are",
    carYearData.length
  );
  console.log(carYearData);
  return carYearData;
}

module.exports = problem5;
